class up1::munin {

  munin::plugin::deploy { 'up1_pastes':
    source => 'up1/munin/up1_pastes'
  }
}
