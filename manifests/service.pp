# Control up1 as a systemd service
class up1::service inherits up1 {

  file { '/etc/systemd/system/up1.service':
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template($up1::systemd_template)
  }

  service { 'up1':
      ensure   => running,
      enable   => true,
      provider => 'systemd',
      require  => File['/etc/systemd/system/up1.service']
  }

  if $up1::use_munin { include up1::munin }
}
