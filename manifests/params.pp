# up1 default packages parameters
class up1::params {
  $repository_remote    = 'https://github.com/Upload/Up1.git'
  $repository_revision  = 'master'
  $default_user         = 'up1'
  $default_group        = 'up1'
  $default_path         = '/srv/up1'
  $systemd_template     = 'up1/systemd.erb'
  $app_api_key          = trocla("${::hostname}_up1_api_key")
  $app_delete_key       = trocla("${::hostname}_up1_delete_key")
  $app_max_file_size    = '50000000'
  $app_email            = ''
  $app_http_port        = '1337'
  $app_https_port       = '0'
  $app_https_cert       = ''
  $app_https_key        = ''
  $cloudflare_token     = ''
  $cloudflare_email     = ''
  $cloudflare_domain    = ''
  $use_munin            = false
}
